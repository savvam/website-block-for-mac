import os # os path, sudo 
import sys # passing command line arguments
import subprocess # callign commands
from termcolor import colored # colorful terminal outputs
import io

try:
   user_input = raw_input
except NameError:
   user_input = input

def main(argv):

  # ensure we are running root
  if os.geteuid() != 0:
    print( colored( "ERROR: program is not running root.\n", 'red') + "Instead run: " + colored("sudo python3 block.py -b ... ", 'green') )
    sys.exit()

  if len(argv) == 1 and argv[0] == '-h':
    print("This is a program for blocking websites. It edits /etc/hosts file ")
    print("Usage:")
    print(colored("sudo python3 block.py -h", 'green'), "\thelp")
    print(colored("sudo python3 block.py -l", 'green'), "\tlist the blocked websites")
    print(colored("sudo python3 block.py -u youtube.com", 'green'), "\tunblock a website")
    print(colored("sudo python3 block.py -b youtube.com", 'green'), "\tblock a website")
    print("Hm, didn't work? You need to restart your DNS cache. Option 1: google it. Here are a few other ideas: try restarting your browser; try deleting browser's cache (many websites cache images on your computer).")

  elif len(argv) == 2 and argv[0] == '-b':
    # you sure you wanna block this website?
    inp = user_input("Block %s? y/n\n" %( colored(argv[1],'yellow') ) )
    while inp != 'y' and inp != 'n':
      inp = user_input("Block %s? y/n\n" %( colored(argv[1],'yellow') ) )
    if inp == 'n':
      print(colored("Exiting, did nothing",'red'))
      sys.exit()
    
    # check if it's already blocked
    f = open("/etc/hosts", "r")
    lines = f.readlines()
    for line in lines:
      if argv[1] in line:
        inp = user_input("Wait, it seems like %s is already blocked; I have the following line:\n%sYou sure you want to add it again? y/n\n" %( colored(argv[1],'yellow'), colored(line,'yellow') ) )
        while inp != 'y' and inp != 'n':
          inp = user_input("Wait, it seems like %s is already blocked; I have the following line:\n%sYou sure you want to add it again? y/n\n" %( colored(argv[1],'yellow'), colored(line,'yellow') ) )
        if inp == 'n':
          print(colored("Exiting, did nothing",'red'))
          sys.exit()

    # append /etc/hosts
    f = open("/etc/hosts", "a")
    f.write("127.0.0.1 " + argv[1] + "\n")
    print("Added %s to the list of blocked websites." %( colored(argv[1],'yellow') ))
    sys.exit()
  
  elif len(argv) == 1 and argv[0] == '-l':
    print(colored("Your list of blocked websites is:",'green'))
    f = open("/etc/hosts", "r")
    lines = f.readlines()
    for line in lines:
      if line[:10] == "127.0.0.1 ":
        print(line[10:-1])

  elif len(argv) == 2 and argv[0] == '-u':
    # per website, ask if it should be unlocked
    f = open("/etc/hosts", "r")
    lines = f.readlines()
    toBeUnblocked = []
    for i in range(len(lines)):
      line = lines[i]
      if line[:10] == "127.0.0.1 ":
        if argv[1] in line[10:-1]:
          inp = user_input("Unblock %s? y/n\n" %( colored(line[10:-1],'yellow') ) )
          while inp != 'y' and inp != 'n':
            inp = user_input("Unblock %s? y/n\n" %( colored(line[10:-1],'yellow') ) )
          if inp == 'y':
            toBeUnblocked += [i]

    if len(toBeUnblocked) == 0:
      print(colored("Exiting, nothing to unblock",'red'))
      sys.exit()
    else:
      # asking one last time
      print(colored("Are you sure you want to unblock the following websites?", 'yellow'))
      for i in toBeUnblocked:
        print(lines[i][10:-1])
      inp = user_input(colored("Unblock? y/n\n",'yellow'))
      while inp != 'y' and inp != 'n':
        inp = user_input(colored("Unblock? y/n\n",'yellow'))

      # changed your mind
      if inp == 'n':
        print(colored("Exiting, nothing to unblock",'red'))
        sys.exit()

      # get the resulting file content
      newLines = ""
      if inp == 'y':
        for i in range(len(lines)):
          if i not in toBeUnblocked:
            newLines += lines[i]

      # rewrite the file
      f = open("/etc/hosts", "w")
      f.write(newLines)
      # print(newLines)
      print("Updated the list of blocked websites accordingly.")
      sys.exit()

  else:
    print('You\'re doing it wrong, get help by inputting:\npython3 block.py -h\n')
    sys.exit(2)
    


if __name__ == '__main__':
    main(sys.argv[1:])